/*******************************************************************************
 * pisensors
 *
 * db.c:  Functions for managing image files.
 *
 * License: GPL v3
 *
 ******************************************************************************
 * Notes:
 * Being a C file (and not Java), please adhere to the 80 character line
 * length standard in this file.
 ******************************************************************************/
#define DB_C

/* To avoid deprecation warnings from GTK+ 2.x */
#pragma GCC diagnostic ignored "-Wdeprecated-declarations"

#include <stdio.h>
#include <stdlib.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <glib.h>
#include <pibox/utils.h>
#include <pibox/parson.h>

#include "pisensors.h"

#define REMOVE_SENSOR_CMD_S     "wget -q --method=DELETE --header='Accept-Version: %s' --body-data='%s' http://127.0.0.1:%d/set/device"
#define MODIFY_SENSOR_CMD_S     "wget -q --method=POST --header='Accept-Version: %s' --body-data='%s' http://127.0.0.1:%d/set/device"

static pthread_mutex_t dbMutex = PTHREAD_MUTEX_INITIALIZER;

/*
 * Linked lists for files.
 */
static GSList   *fileList = NULL;       // List of files to play
static char     *inbuf;
static char     *dbPath;

#ifdef DEBUG
/*
 * Debug
 */
gint 
printEntry(gconstpointer item, gconstpointer user_data)
{
    char *json   = (char *)item;
    piboxLogger(LOG_INFO, "DUMP: %s\n", json);
    return 0;
}

static void
dumpList()
{
    g_slist_foreach(fileList, (GFunc)printEntry, NULL);
}

#else

#define dumpList()
#endif

/*
 *========================================================================
 * Name:   findByIP
 * Prototype:  void findByIP( gconstpointer item, gconstpointer user_data )
 *
 * Description:
 * Find the entry in fileList with the specified IP.
 *
 * Arguments:
 * gcostpointer *item       An item to test.
 * gcostpointer user_data   The object to test against the item.
 *
 * Returns:
 * 1 if not a match.
 * 0 if the item is a match.
 *========================================================================
 */
gint 
findByIP(gconstpointer item, gconstpointer user_data)
{
    int             rc = 1;
    JSON_Value      *root_value;
    JSON_Object     *root_object;

    const char *sensor_ipaddr;
    char *ipaddr = (char *)user_data;
    char *json   = (char *)item;

    if ( item == NULL )
        return rc;
    if ( user_data == NULL )
        return rc;

    piboxLogger(LOG_TRACE3, "Item: %s\n", json);

    if ( ipaddr != NULL )
    {
        root_value = json_parse_string(json);
        root_object = json_value_get_object(root_value);
        sensor_ipaddr = json_object_get_string (root_object, "ipaddr");
        if ( sensor_ipaddr != NULL )
        {
            if ( strncmp(sensor_ipaddr, ipaddr, strlen(sensor_ipaddr)) == 0 )
            {
                piboxLogger(LOG_TRACE3, "IP addresses match!\n");
                rc = 0;
            }
        }
        else
            piboxLogger(LOG_ERROR, "Sensor address is missing from item: %s\n", json);
        json_value_free(root_value);
    }
    else
        piboxLogger(LOG_ERROR, "IP address is missing from request.\n");
    return rc;
}

/*
 *========================================================================
 * Name:   readFile
 * Prototype:  void readFile( char * )
 *
 * Description:
 * Read the specified sensor file and save it into the list of files.
 *
 * Arguments:
 * char *filename       Name of file (with path) to load.
 *========================================================================
 */
static void
readFile(char *filename)
{
    struct stat     stat_buf;
    FILE            *fd;
    char            *entry;
    char            *ptr;
    JSON_Value      *root_value;
    JSON_Object     *root_object;

    if ( stat(filename, &stat_buf) != 0 )
    {
        piboxLogger(LOG_ERROR, "Error stat of file: %s, reason = %d\n", filename, errno);
        return;
    }

    /* Get the basename of the path */
    ptr = rindex(filename, '/');
    if ( ptr == NULL )
    {
        piboxLogger(LOG_ERROR, "Can't find basename of path: %s\n", filename);
        return;
    }
    ptr++;

    if ( stat_buf.st_size > 0 )
    {
        fd = fopen(filename, "r");
        if ( fd != NULL )
        {
            /* Read the file */
            inbuf = g_malloc0(stat_buf.st_size+2);
            fgets(inbuf, stat_buf.st_size+1, fd);

            /* Parse it as JSON, then add the file name as it's IP address. */
            root_value = json_parse_string(inbuf);
            root_object = json_value_get_object(root_value);
            json_object_set_string(root_object, "ipaddr", ptr);

            /* Convert back to a string */
            entry = json_serialize_to_string(root_value);

            /* And append to the list of entries. */
            piboxLogger(LOG_TRACE2, "Adding to fileList: %s\n", entry);
            fileList = g_slist_append(fileList, entry);
            json_value_free(root_value);
            free(inbuf);
            fclose(fd);
        }
        else
        {
            piboxLogger(LOG_ERROR, "Failed fopen on %s: reason = %s\n", filename, strerror(errno));
        }
    }
    else
    {
        piboxLogger(LOG_ERROR, "Failed fopen on %s: st_size = 0\n", filename);
    }
}

/*
 *========================================================================
 * Name:   getFiles
 * Prototype:  void getFiles( char * )
 *
 * Description:
 * Generate a list of image files.
 *
 * Arguments:
 * char *path       Top of database (IoT files)
 *========================================================================
 */
static void 
getFiles ( char *path )
{
    DIR             *dir;
    char            filename[PATH_MAX];
    struct dirent   *ent;

    if ( (dir = opendir (path)) == NULL)
    {
        piboxLogger(LOG_ERROR, "Can't open IoT directory: %s\n", path);
        return;
    }

    while ((ent = readdir (dir)) != NULL)
    {   
        if ( strncmp(ent->d_name, ".", 1) == 0 )
            continue;

        /* Read the file into a movie struct. */
        sprintf(filename, "%s/%s", path, ent->d_name);
        piboxLogger(LOG_INFO, "json file: %s\n", filename);
        readFile(filename);
    }

    closedir(dir);
}

/*
 *========================================================================
 * Name:   dbLoad
 * Prototype:  void dbLoad( char * )
 *
 * Description:
 * Load the database into a local link list.
 *
 * Arguments:
 * char *path       Top of database (IoT files)
 *
 * Notes:
 * We changed to the top of the db directory tree when we started, so 
 * we just search for files from the current location.
 *========================================================================
 */
void 
dbLoad(char *path)
{
    /*
     * Create list of files to play.
     */
    getFiles(path);

    /* Save the path - other functions will need it */
    dbPath = path;
}

/*
 *========================================================================
 * Name:   dbCount
 * Prototype:  int dbCount( void )
 *
 * Description:
 * Return the size of the database.
 *========================================================================
 */
int
dbCount( void )
{
    int count;
    pthread_mutex_lock( &dbMutex );
    count = g_slist_length(fileList);
    pthread_mutex_unlock( &dbMutex );
    return count;
}

/*
 *========================================================================
 * Name:   dbGetEntry
 * Prototype:  void dbGetEntry( gint idx )
 *
 * Description:
 * Retrieve the app entry at the specified index.
 *========================================================================
 */
char *
dbGetEntry (gint idx)
{
    char *entry;
    pthread_mutex_lock( &dbMutex );
    entry = (char *)g_slist_nth_data(fileList, idx);
    pthread_mutex_unlock( &dbMutex );
    return entry;
}

/*========================================================================
 * Name:   dbModifyEntry
 * Prototype:  void dbModifyEntry( char *entry, int delete, int state )
 *
 * Description:
 * This function notifies imrest to modify or remove an entry.  We'll pick up the
 * change when the watcher sees the sensor file updated.
 *
 * Arguments:
 * char *entry      The entry to by updated.
 * int  delete      If set, delete the entry instead of modifying it.
 * int  state       If not deleting, then set the state to this value.
 *
 * Notes:
 * This function uses calls to imrest's API to make the changes.
 *========================================================================*/
void
dbModifyEntry( char *entry, int delete, int state )
{
    JSON_Value      *root_value;
    JSON_Object     *root_object;
    char            *new_entry;
    char            *json;
    char            *uuid;
    char            *cmd;
    int             len;
    char            buf[32];;

    /* Remove IPaddr from the entry */
    json = strdup(entry);
    root_value = json_parse_string(json);
    root_object = json_value_get_object(root_value);
    uuid = strdup( json_object_get_string (root_object, "uuid") );
    json_value_free(root_value);

    /* Get a lock on the db. */
    pthread_mutex_lock( &dbMutex );

    if ( delete )
    {
        root_value = json_value_init_object();
        root_object = json_value_get_object(root_value);
        json_object_set_string(root_object, "command", "delete");
        json_object_set_string(root_object, "uuid", uuid);
        new_entry = json_serialize_to_string(root_value);

        len = strlen(REMOVE_SENSOR_CMD_S)*2 + strlen(new_entry);
        cmd = g_malloc0(len);
        sprintf(cmd, REMOVE_SENSOR_CMD_S, IMREST_VERSION, new_entry, IMREST_PORT);
        piboxLogger(LOG_TRACE2, "Removing sensor: cmd = %s\n", cmd);

        /* Tell imrest to delete it. We don't encrypt because it accepts unencrypted from localhost. */
        system(cmd);
        free(cmd);
        json_free_serialized_string(new_entry);
        json_value_free(root_value);
    }
    else
    {
        root_value = json_value_init_object();
        root_object = json_value_get_object(root_value);
        json_object_set_string(root_object, "command", "update");
        json_object_set_string(root_object, "uuid", uuid);
        memset(buf,0,32);
        if ( state == 0 )
            sprintf(buf, "Off");
        else
            sprintf(buf, "On");
        json_object_set_string(root_object, "state", buf);
        new_entry = json_serialize_to_string(root_value);

        len = strlen(MODIFY_SENSOR_CMD_S)*2 + strlen(new_entry);
        cmd = g_malloc0(len);
        sprintf(cmd, MODIFY_SENSOR_CMD_S, IMREST_VERSION, new_entry, IMREST_PORT);
        piboxLogger(LOG_TRACE2, "Updating sensor: cmd = %s\n", cmd);

        /* Tell imrest to update it. We don't encrypt because it accepts unencrypted from localhost. */
        system(cmd);
        free(cmd);
        json_free_serialized_string(new_entry);
        json_value_free(root_value);
    }

    /* Release the lock on the db. */
    pthread_mutex_unlock( &dbMutex );

    /* Clean up JSON artifacts */
    free(uuid);
    free(json);
}

/*========================================================================
 * Name:   dbReloadEntry
 * Prototype:  void dbReloadEntry( const char *ipaddr, int remove )
 *
 * Description:
 * This gets called if the watcher discovers a file has been modified,
 * added or removed.
 *
 * Arguments:
 * char *ipaddr     The ip address of the file that has been modified, added, etc.
 * int  remove      If set, the watcher noticed that this sensor has been removed.
 *========================================================================*/
void
dbReloadEntry( const char *path, int add )
{
    GSList          *item = NULL;
    char            *ptr = NULL;
    char            *arg = NULL;

    /* Generate the filename */
    ptr = rindex(path, '/');
    if ( ptr == NULL )
    {
        piboxLogger(LOG_ERROR, "Can't find basename of path: %s\n", path);
        return;
    }
    ptr++;
    piboxLogger(LOG_INFO, "Looking for ipaddr: %s\n", ptr);

    /* Get a lock on the db. */
    pthread_mutex_lock( &dbMutex );

    /* First, try to remove the existing entry if any. */
    item = g_slist_find_custom(fileList, ptr, (GCompareFunc)findByIP);
    if ( item != NULL )
    {
        piboxLogger(LOG_INFO, "Removing sensor, ipaddr = %s, item = %s\n", ptr, (char *)item->data);
        fileList = g_slist_delete_link(fileList, item);
    }
    dumpList();

    /* If it's a new or modified file, add it to the db. */
    if ( add == 1 )
    {
        piboxLogger(LOG_INFO, "Adding to fileList: %s\n", ptr);
        arg = strdup(path);
        readFile(arg);
        free(arg);
    }

    /* Release the lock on the db. */
    pthread_mutex_unlock( &dbMutex );
}

