/*******************************************************************************
 * pisensors
 *
 * pisensors.c:  program main
 *
 * License: GPL v3
 *
 ******************************************************************************
 * Notes:
 * Being a C file (and not Java), please adhere to the 80 character line
 * length standard in this file.
 ******************************************************************************/
#define PISENSORS_C

/* To avoid deprecation warnings from GTK+ 2.x */
#pragma GCC diagnostic ignored "-Wdeprecated-declarations"

#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <stdint.h>
#include <unistd.h>
#include <string.h>
#include <errno.h>
#include <cairo.h>
#include <gtk/gtk.h>
#include <gdk/gdkkeysyms.h>
#include <pibox/pibox.h>
#include <pibox/parson.h>

#include "pisensors.h"

static pthread_mutex_t uiMutex = PTHREAD_MUTEX_INITIALIZER;
static pthread_mutex_t touchMutex = PTHREAD_MUTEX_INITIALIZER;

/* Local prototypes */
static void removeSensor(char *entry);
static void toggleState(char *entry);
static gboolean draw_icon(GtkWidget *widget, GdkEventExpose *event, gpointer user_data);
static gboolean draw_nav(GtkWidget *widget, GdkEventExpose *event, gpointer user_data);
static gboolean key_press(GtkWidget *widget, GdkEventKey *event, gpointer user_data);
gboolean imageTouchGTK( gpointer arg );

GtkWidget       *window;
GtkWidget       *sensorTable;
GtkWidget       *navTable;
GtkWidget       *darea[12];
GtkWidget       *sarea[3];
cairo_surface_t *image = NULL;
cairo_t         *cr = NULL;
guint           image_cycle_id = 0;
int             inprogress = 0;
int             page = 0;
int             maxRow = 0;
time_t          lastUpdate;
gint            navHeight = 100;
gint            sensorHeight = 0;
gint            sensorWidth = 0;
int             numRows = 3;
int             numCols = 3; // Icon, State, Remove

// The home key is the key that exits the app.
guint homeKey = -1;

// Flag for the watcher to notify us to update the UI.
guint enableUIUpdate = 0;

// Cell size information.
guint cellWidth = 0;
guint cellHeight = 0;

/*
 *========================================================================
 * Name:   pisensorsExit
 * Prototype:  void pisensorsExit( void )
 *
 * Description:
 * Graceful exit of the app.
 *========================================================================
 */
void
pisensorsExit( void )
{
    gtk_main_quit();
}

/*
 *========================================================================
 * Name:   imageTouch
 * Prototype:  void imageTouch( REGION_T *pt )
 *
 * Description:
 * Handler for absolute touch reports.  We need to compute where on the
 * screen the touch occurs so we know which handler to call.
 *
 *     Icons     State      Remove
 *   --------------------------------
 *   |   1    |    2      |    3    | 
 *   --------------------------------
 *   |   4    |    5      |    6    |
 *   --------------------------------
 *   |   7    |    8      |    9    |
 *   --------------------------------
 *   | <  10  | stop  11  | >  12   | Nav Bar
 *   --------------------------------
 *========================================================================
 */
void
imageTouch( REGION_T *pt )
{
    g_idle_add( (GSourceFunc)imageTouchGTK, (gpointer)pt );
}

gboolean
imageTouchGTK( gpointer arg )
{
    REGION_T    *pt = (REGION_T *)arg;
    int         cellWidth, cellHeight;
    int         col, row;
    int         region;
    char        *entry;

    /* Prevent multiple touches from being handled at the same time. */
    pthread_mutex_lock( &touchMutex );
    if ( inprogress )
    {
        piboxLogger(LOG_INFO, "In progress, skipping update\n");
        pthread_mutex_unlock( &touchMutex );
        return FALSE;
    }
    inprogress = 1;
    pthread_mutex_unlock( &touchMutex );

    /*
     * Find the cell of the touch.  Note that the nav bar is not the same
     * height as the table cells for the sensors.
     */
    if ( pt->y < sensorHeight )
    {
        /* This is the sensor table */
        cellWidth = sensorWidth / numCols; 
        if ( pt->x > (cellWidth*2) )
            col = 3;
        else if ( pt->x > cellWidth )
            col = 2;
        else
            col = 1;

        cellHeight = sensorHeight / numRows; 
        if ( pt->y > (cellHeight*2) )
            row = 2;
        else if ( pt->y > cellHeight )
            row = 1;
        else
            row = 0;

        region = (row * 3) + col;

    }
    else
    {
        /* This is the nav bar */
        cellWidth = sensorWidth / 3; 
        if ( pt->x > (cellWidth*2) )
            col = 3;
        else if ( pt->x > cellWidth )
            col = 2;
        else
            col = 1;

        region = 9 + col;
    }

    switch( region )
    {
        /* Ignore the cells with the sensor icons. */
        case 1: piboxLogger(LOG_INFO, "Cell 1\n"); break;
        case 4: piboxLogger(LOG_INFO, "Cell 4\n"); break;
        case 7: piboxLogger(LOG_INFO, "Cell 7\n"); break;

        /* Change states for selected sensors. */
        case 2: 
                piboxLogger(LOG_INFO, "Cell 2: Toggle 1\n"); 
                entry = dbGetEntry((int)((page*3)+row));
                if ( entry != NULL )
                    toggleState(entry);
                break;
        case 5: 
                piboxLogger(LOG_INFO, "Cell 5: Toggle 2\n"); 
                entry = dbGetEntry((int)((page*3)+row));
                if ( entry != NULL )
                    toggleState(entry);
                break;
        case 8: 
                piboxLogger(LOG_INFO, "Cell 8: Toggle 3\n"); 
                entry = dbGetEntry((int)((page*3)+row));
                if ( entry != NULL )
                    toggleState(entry);
                break;

        /* Remove selected sensors. */
        case 3: 
                piboxLogger(LOG_INFO, "Cell 3: Remove 1\n"); 
                entry = dbGetEntry((int)((page*3)+row));
                if ( entry != NULL )
                    removeSensor(entry);
                break;
        case 6: 
                piboxLogger(LOG_INFO, "Cell 6: Remove 2\n"); 
                entry = dbGetEntry((int)((page*3)+row));
                if ( entry != NULL )
                    removeSensor(entry);
                break;
        case 9: 
                piboxLogger(LOG_INFO, "Cell 9: Remove 3\n"); 
                entry = dbGetEntry((int)((page*3)+row));
                if ( entry != NULL )
                    removeSensor(entry);
                break;

        /* Handle nav bar selections. */
        case 10: 
                piboxLogger(LOG_INFO, "Cell 10: Back\n"); 
                if ( maxRow>((page+1)*3) )
                {
                    // Left is back
                    page++;
                }
                gtk_widget_queue_draw(GTK_WIDGET(sensorTable));
                break;
        case 11: 
                piboxLogger(LOG_INFO, "Cell 11: Stop\n"); 
                pisensorsExit();
                break;
        case 12: 
                piboxLogger(LOG_INFO, "Cell 12: Forward\n"); 
                if ( page>0 )
                {
                    // Right is forward
                    page--;
                }
                gtk_widget_queue_draw(GTK_WIDGET(sensorTable));
                break;

        default:
            piboxLogger(LOG_ERROR, "Invalid region: %d\n", region);
            break;
    }

    pthread_mutex_lock( &touchMutex );
    inprogress = 0;
    pthread_mutex_unlock( &touchMutex );
    return(FALSE);
}

/*
 *========================================================================
 * Name:   loadKeysyms
 * Prototype:  void loadKeysyms( void )
 *
 * Description:
 * Read in the keysym file so we know how the platform wants us to behave.
 *
 * Notes:
 * Format is KEYSYM NAME:ACTION
 *========================================================================
 */
void
loadKeysyms( void )
{
    char        *tsave = NULL;
    struct stat stat_buf;
    char        *keysym;
    char        *action;
    char        *ptr;
    FILE        *fd;
    char        buf[128];
    char        *path;

    if ( isCLIFlagSet( CLI_TEST) )
        path = KEYSYMS_FD;
    else
        path = KEYSYMS_F;

    // Read in /etc/pibox-keysysm
    if ( stat(path, &stat_buf) != 0 )
    {
        piboxLogger(LOG_INFO, "No keysym file: %s\n", path);
        return;
    }

    fd = fopen(path, "r");
    if ( fd == NULL )
    {
        piboxLogger(LOG_ERROR, "Failed to open keysyms file: %s - %s\n", path, strerror(errno));
        return;
    }

    memset(buf, 0, 128);
    while( fgets(buf, 127, fd) != NULL )
    {
        // Ignore comments
        if ( buf[0] == '#' )
            continue;

        // Strip leading white space
        ptr = buf;
        ptr = piboxTrim(ptr);

        // Ignore blank lines
        if ( strlen(ptr) == 0 )
            continue;

        // Strip newline
        piboxStripNewline(ptr);

        // Grab first token
        keysym = strtok_r(ptr, ":", &tsave);
        if ( keysym == NULL )
            continue;

        // Grab second token
        action = strtok_r(NULL, ":", &tsave);
        if ( action == NULL )
            continue;

        piboxLogger(LOG_INFO, "keysym / action: %s / %s \n", keysym, action);

        // Set the home key
        if ( strncasecmp("home", action, 4) == 0 )
        {
            homeKey = gdk_keyval_from_name(keysym);
            piboxLogger(LOG_INFO, "homeKey = %08x\n", homeKey);
        }
    }
}

/*
 *========================================================================
 * Name:   loadDisplayConfig
 * Prototype:  void loadDisplayConfig( void )
 *
 * Description:
 * Read in the display config file so we know how the display should be 
 * handled.
 *
 * Notes:
 * Format is 
 *     DISPLAY TYPE (DVT LCD)
 *     RESOLUTION
 *========================================================================
 */
void
loadDisplayConfig( void )
{
    int width;
    int height;

    piboxLoadDisplayConfig();
    width = piboxGetDisplayWidth();
    height = piboxGetDisplayHeight();
    if ( (width<=800) || (height<=480) )
    {
        setCLIFlag( CLI_SMALL_SCREEN );
    }

    if ( piboxGetDisplayType() == PIBOX_LCD )
    {
        setCLIFlag(CLI_TOUCH);
    }
    else
    {
        unsetCLIFlag(CLI_TOUCH);
    }
}

/*
 *========================================================================
 * Name:   toggleState
 * Prototype:  void toggleState( int idx )
 *
 * Description:
 * Toggles the state for the specified sensor.
 *
 * Arguments:
 * int idx      The sensor to update.
 *========================================================================
 */
static void
toggleState(char *entry)
{
    JSON_Value      *root_value;
    JSON_Object     *root_object;
    int             num;

    /* Extract the current state */
    root_value = json_parse_string(entry);
    root_object = json_value_get_object(root_value);
    num = (int)json_object_get_number(root_object, "state");
    json_value_free(root_value);

    /* Toggle the value */
    num = !num;

    /* Send the update. */
    piboxLogger(LOG_INFO, "Toggling state to %d for sensor @ %s\n", num, entry);
    dbModifyEntry(entry, 0, num);
}

/*
 *========================================================================
 * Name:   remoteSensor
 * Prototype:  void remoteSensor( int idx )
 *
 * Description:
 * Removes the specified sensor (it will have to pair again later).
 *
 * Arguments:
 * int idx      The sensor to remove.
 *========================================================================
 */
static void
removeSensor(char *entry)
{
    /* Send the update. */
    piboxLogger(LOG_INFO, "Remove sensor @ %s\n", entry);
    dbModifyEntry(entry, 1, 0);
}

/*
 *========================================================================
 * Name:   button_press
 * Prototype:  void button_press( GtkWidget *, GdkEventExpose *, gpointer )
 *
 * Description:
 * Catches a double click in the poster to start an application.
 *========================================================================
 */
static gboolean
button_press(GtkWidget *widget, GdkEventButton *event, gpointer user_data)
{
    char     *entry;
    gint     row = (int)(GPOINTER_TO_INT(user_data)/3);
    gint     col = (int)(GPOINTER_TO_INT(user_data)%3);

    piboxLogger(LOG_INFO, "Button[%d]: page %d, row %d, col %d\n", GPOINTER_TO_INT(user_data), page+1, row, col);

    switch(event->type) {
        case GDK_BUTTON_PRESS:
            if ( event->button == 1 )
            {
                entry = dbGetEntry((int)((page*3)+row));
                if (entry == NULL)
                    return(TRUE);

                if ( col == 1 )
                    toggleState(entry);
                else if ( col == 2 )
                    removeSensor(entry);

                return(TRUE);
            }
            break;
        default:
            break;
    }
    return(TRUE);
}

/*
 *========================================================================
 * Name:   button_press_nav
 * Prototype:  void button_press( GtkWidget *, GdkEventExpose *, gpointer )
 *
 * Description:
 * Catches a click on the nav buttons.
 *========================================================================
 */
static gboolean
button_press_nav(GtkWidget *widget, GdkEventButton *event, gpointer user_data)
{
    gint idx = (int)(GPOINTER_TO_INT(user_data));
    piboxLogger(LOG_INFO, "nav[%d]\n", idx);

    switch(event->type) {
        case GDK_BUTTON_PRESS:
            if ( event->button == 1 )
            {
                if ( (idx == 1) && (maxRow>((page+1)*3)) )
                {
                    // Left is back
                    page++;
                }
                else if ( (idx == 0) && (page>0) )
                {
                    // Right is back forward
                    page--;
                }
                gtk_widget_queue_draw(GTK_WIDGET(sensorTable));
                return(TRUE);
            }
            break;
        default:
            break;
    }
    return(TRUE);
}

/*
 *========================================================================
 * Name:   key_press
 * Prototype:  void key_press( GtkWidget *, GdkEventExpose *, gpointer )
 *
 * Description:
 * Handles exiting the application via keystrokes.
 *========================================================================
 */
static gboolean
key_press(GtkWidget *widget, GdkEventKey *event, gpointer user_data)
{
    switch(event->keyval)
    {
        case GDK_KEY_Q:
        case GDK_KEY_q:
            if (event->state & GDK_CONTROL_MASK)
            {
                piboxLogger(LOG_INFO, "Ctrl-Q key\n");
                pisensorsExit();
                return(TRUE);
            }
            break;

        case GDK_KEY_Home:
            piboxLogger(LOG_INFO, "Home key\n");
            pisensorsExit();
            return(TRUE);
            break;

        case GDK_KEY_Left:
        case GDK_KEY_KP_Left:
            piboxLogger(LOG_INFO, "Prev image\n");
            return(TRUE);
            break;

        case GDK_KEY_Right:
        case GDK_KEY_KP_Right:
            piboxLogger(LOG_INFO, "Next image\n");
            return(TRUE);
            break;

        case GDK_KEY_space:
        case GDK_KEY_Return:
        case GDK_KEY_KP_Space:
        case GDK_KEY_KP_Enter:
            piboxLogger(LOG_INFO, "Next image\n");
            return(TRUE);
            break;

        default:
            if ( event->keyval == homeKey )
            {
                piboxLogger(LOG_INFO, "Keysym configured home key\n");
                gtk_main_quit();
                return(TRUE);
            }
            else
            {
                piboxLogger(LOG_INFO, "Unknown keysym: %s\n", gdk_keyval_name(event->keyval));
                return(FALSE);
            }
            break;
    }
    return(FALSE);
}

#if 0
/*
 * ========================================================================
 * Name:   sigHandler
 * Prototype:  static void sigHandler( int sig, siginfo_t *si, void *uc )
 *
 * Description:
 * Handles program signals.
 *
 * Input Arguments:
 * int       sig    Signal that cause callback to be called
 * siginfo_t *si    Structure that includes timer over run information and timer ID
 * void      *uc    Unused but required with sigaction handlers.
 * ========================================================================
 */
static void
sigHandler( int sig, siginfo_t *si, void *uc )
{
    switch (sig)
    {
        case SIGHUP:
            /* Reset the application. */
            break;

        case SIGTERM:
            /* Bring it down gracefully. */
            break;
    }
}
#endif

/*
 *========================================================================
 * Name:   do_drawing
 * Prototype:  void do_drawing( GtkWidget *, char * )
 *
 * Description:
 * Updates the sensor icon area of the display.
 * 
 * Arguments:
 * GtkWidget *      The widget in which we'll do the drawing           
 * char *           The path to the image file to fill in the drawing.
 *========================================================================
 */
void 
do_drawing( GtkWidget *drarea, char *path )
{
    gint            width, height;
    gint            swidth, sheight;
    gint            awidth, aheight;
    gdouble         offset_x, offset_y;
    GtkRequisition  req;
    GdkPixbuf       *newimage;
    gfloat          ri, rs;
    GdkPixbuf       *image = NULL;
    struct stat     stat_buf;

    if ( stat(path, &stat_buf) != 0 )
    {
        piboxLogger(LOG_ERROR, "Can't find icon: %s\n", path);
        return;
    }

    cr = gdk_cairo_create(drarea->window);
    req.width = gdk_window_get_width(drarea->window);
    req.height = gdk_window_get_height(drarea->window);
    awidth = (int)(req.width * .9);
    aheight = (int)(req.height * .9);
    piboxLogger(LOG_TRACE1, "window w/h: %d / %d\n", req.width, req.height);
    piboxLogger(LOG_TRACE1, "adjusted window w/h: %d / %d\n", awidth, aheight);

    /* Load the icon. */
    image  = gdk_pixbuf_new_from_file(path, NULL);
    width  = gdk_pixbuf_get_width(image);
    height = gdk_pixbuf_get_height(image);

    piboxLogger(LOG_TRACE1, "Icon image: %s: w/h: %d / %d \n", path, width, height);

    /* Compute scaling to keep aspect ratio but fit in the screen */
    ri = (gfloat)((gfloat)width / (gfloat)height);
    rs = (gfloat)((gfloat)awidth / (gfloat)aheight);
    if ( rs > ri )
    {
        piboxLogger(LOG_TRACE1, "rs > ri\n");
        swidth = (gint)(width * aheight/height);
        sheight = aheight;
    }
    else
    {
        piboxLogger(LOG_TRACE1, "rs <= ri\n");
        swidth = awidth;
        sheight = (gint)(height * awidth/width);
    }
    piboxLogger(LOG_TRACE1, "updated icon size: %d / %d\n", swidth, sheight);
    newimage = gdk_pixbuf_scale_simple(image, swidth, sheight, GDK_INTERP_BILINEAR);

    /* Positioning */
    offset_x = (double)(req.width - swidth) / (double)2.0;
    offset_y = (double)(req.height - sheight) / (double)2.0;
    gdk_cairo_set_source_pixbuf(cr, newimage, offset_x, offset_y);
    g_object_unref(image);
    g_object_unref(newimage);

    cairo_paint(cr);    
    cairo_destroy(cr);
    cr = NULL;
    image = NULL;
}

/*
 *========================================================================
 * Name:   draw_icon
 * Prototype:  void draw_icon( GtkWidget *, GdkEventExpose *, gpointer )
 *
 * Description:
 * Updates the the sensors display when realize and expose events occur.
 *========================================================================
 */
static gboolean
draw_icon(GtkWidget *widget, GdkEventExpose *event, gpointer user_data)
{
    gint            idx = GPOINTER_TO_INT(user_data);
    gint            field;
    char            *entry;
    char            *value;
    double          number;
    char            *icon_path = NULL;
    JSON_Value      *root_value;
    JSON_Object     *root_object;

    /* Which field in a row are we going to draw? */
    field = idx%3;

    /* Get the app entry for this index */
    piboxLogger(LOG_TRACE1, "Entry ID: %d\n", (int)(idx/3));
    entry = dbGetEntry((int)((page*3)+(idx/3)));
    if (entry == NULL )
    {
        return FALSE;
    }

    /* Convert to JSON */
    piboxLogger(LOG_TRACE1, "Entry: %s\n", entry);
    root_value = json_parse_string(entry);
    root_object = json_value_get_object(root_value);

    /* Is this the sensor icon field? */
    if ( field == 0 )
    {
        value = (char *)json_object_get_string( root_object, "type" );
        if ( value == NULL )
        {
            piboxLogger(LOG_ERROR, "Can't find \"type\" field in sensor configuration.\n");
            json_value_free(root_value);
            return FALSE;
        }

        /* Determine icon to use for this entry. */
        icon_path=getIconPath(value);
        if ( icon_path == NULL )
        {
            piboxLogger(LOG_ERROR, "Unrecognized \"type\" field: %s.\n", value);
            json_value_free(root_value);
            return FALSE;
        }
    }

    /* Is this the state icon field? */
    else if ( field == 1 )
    {
        number = json_object_get_number( root_object, "state" );
        if ( number == 0 )
        {
            icon_path=getIconPath("disabled");
        }
        else 
        {
            icon_path=getIconPath("enabled");
        }
    }

    /* Then it must be the remove field. */
    else 
    {
        icon_path=getIconPath("remove");
    }

    /* Cleanup */
    json_value_free(root_value);

    /* Draw the appropriate icon */
    if ( icon_path != NULL )
    {
        do_drawing(darea[idx], icon_path);
        free(icon_path);
    }
    return(FALSE);
}

/*
 *========================================================================
 * Name:   draw_nav
 * Prototype:  void draw_nav( GtkWidget *, GdkEventExpose *, gpointer )
 *
 * Description:
 * Updates the poster area of the display when realize and expose events occur.
 *========================================================================
 */
static gboolean
draw_nav(GtkWidget *widget, GdkEventExpose *event, gpointer user_data)
{
    gint            idx = GPOINTER_TO_INT(user_data);
    char            *icon_path = NULL;

    switch(idx)
    {
        /* We don't error check this - we know how many we can have. */
        case 0: icon_path=getIconPath("left-arrow"); break;
        case 1: icon_path=getIconPath("stop-button");break;
        case 2: icon_path=getIconPath("right-arrow");break;
    }

    do_drawing(sarea[idx], icon_path);
    free(icon_path);
    return(FALSE);
}

/*
 *========================================================================
 * Name:   updateUIScheduler
 * Prototype:  gboolean updateUIScheduler( gpointer data )
 *
 * Description:
 * If the update UI flag is set then schedule a UI update.
 *========================================================================
 */
gboolean
updateUIScheduler( gpointer data )
{
    time_t      now;

    /* Get current time. */
    now = time(NULL);

    /* Get a lock on the db. */
    pthread_mutex_lock( &uiMutex );

    if ( enableUIUpdate )
    {
        piboxLogger(LOG_TRACE1, "Wather is forcing widget refresh.\n");
        gtk_widget_queue_draw(GTK_WIDGET(sensorTable));
        enableUIUpdate = 0;
        lastUpdate = now;
    }
    else
    {
        /* If we haven't updated in 5 seconds, do an update. */
        piboxLogger(LOG_TRACE1, "now: %ld, last: %ld\n", (long)now, (long)lastUpdate);
        if ( (now - lastUpdate) > 5 )
        {
            piboxLogger(LOG_TRACE1, "Five second widget refresh.\n");
            gtk_widget_queue_draw(GTK_WIDGET(sensorTable));
            lastUpdate = now;
        }
    }

    /* Release the lock on the db. */
    pthread_mutex_unlock( &uiMutex );

    return TRUE;
}


/*
 *========================================================================
 * Name:   updateUI
 * Prototype:  void updateUI( void )
 *
 * Description:
 * Sets the update flag for the UI.
 *========================================================================
 */
void
updateUI()
{
    /* Get a lock on the db. */
    pthread_mutex_lock( &uiMutex );

    enableUIUpdate = 1;

    /* Release the lock on the db. */
    pthread_mutex_unlock( &uiMutex );
}

/*
 *========================================================================
 * Name:   createWindow
 * Prototype:  GtkWidget *createWindow( void )
 *
 * Description:
 * Creates the main window with a list of videos on the left and currently
 * selected video poster on the right.  The poster area is sensitive to
 * clicks that will start playing the video.
 *========================================================================
 */
GtkWidget *
createWindow()
{
    GtkWidget           *vbox;
    GtkWidget           *dwga;
    int                 idx, row, col;
    gint                windowWidth = 0;
    gint                windowHeight = 0;

    window = gtk_window_new(GTK_WINDOW_TOPLEVEL);
    GTK_WIDGET_SET_FLAGS(window, GTK_CAN_FOCUS );
    gtk_widget_add_events(window, GDK_KEY_PRESS_MASK);

    if ( piboxGetDisplayType() != PIBOX_LCD )
    {
        g_signal_connect(G_OBJECT(window),
                "key_press_event",
                G_CALLBACK(key_press),
                NULL);
    }

    vbox = gtk_vbox_new (FALSE, 10);
    gtk_widget_set_name (vbox, "vbox");
    gtk_container_add (GTK_CONTAINER (window), vbox);

    windowWidth=piboxGetDisplayWidth();
    if ( windowWidth == 0 )
        windowWidth=800;

    windowHeight=piboxGetDisplayHeight();
    if ( windowHeight == 0 )
        windowHeight=480;

    /* Save dimensions - we need them for touchscreen computations. */
    sensorWidth=windowWidth;
    sensorHeight=windowHeight-navHeight;

    /* Three rows showing three sensors at a time. */
    sensorTable = gtk_table_new(numRows,numCols,TRUE);
    gtk_widget_set_size_request(sensorTable, windowWidth, sensorHeight);
    gtk_box_pack_start (GTK_BOX (vbox), sensorTable, TRUE, TRUE, 0);

    /*
     * Drawing areas for each cell of the table.
     */
    cellWidth = windowWidth / numCols;
    cellHeight = (windowHeight-navHeight) / numRows;
    memset(darea, 0, sizeof(darea));
    idx = 0;
    for (row=0; row<numRows; row++)
    {
        for (col=0; col<numCols; col++)
        {
            dwga = gtk_drawing_area_new();
            g_signal_connect(G_OBJECT(dwga), "expose_event", G_CALLBACK(draw_icon), GINT_TO_POINTER(idx));

            /* Don't use button events if the touchscreen is in use. */
            if ( piboxGetDisplayType() != PIBOX_LCD )
            {
                gtk_widget_add_events(GTK_WIDGET(dwga), GDK_BUTTON_PRESS_MASK);
                g_signal_connect(G_OBJECT(dwga),
                    "button_press_event",
                    G_CALLBACK(button_press),
                    GINT_TO_POINTER(idx));
            }

            gtk_widget_set_size_request(dwga, cellWidth, cellHeight);
            gtk_table_attach_defaults(GTK_TABLE(sensorTable), dwga, col, col+1, row, row+1 );
            darea[idx] = dwga;
            idx++;
        }
    }

    /* Two buttons to scroll through the sensors */ 
    navTable = gtk_table_new(1,3,FALSE);
    gtk_widget_set_size_request(navTable, windowWidth, navHeight);
    gtk_box_pack_end (GTK_BOX (vbox), navTable, TRUE, TRUE, 0);

    memset(sarea, 0, sizeof(sarea));
    for (col=0; col<3; col++)
    {
        dwga = gtk_drawing_area_new();
        g_signal_connect(G_OBJECT(dwga), "expose_event", G_CALLBACK(draw_nav), GINT_TO_POINTER(col));

        /* Don't use button events if the touchscreen is in use. */
        if ( piboxGetDisplayType() != PIBOX_LCD )
        {
            gtk_widget_add_events(GTK_WIDGET(dwga), GDK_BUTTON_PRESS_MASK);
            g_signal_connect(G_OBJECT(dwga),
                "button_press_event",
                G_CALLBACK(button_press_nav),
                GINT_TO_POINTER(col));
        }

        gtk_widget_set_size_request(dwga, navHeight-5, navHeight-5);
        gtk_table_attach_defaults(GTK_TABLE(navTable), dwga, col, col+1, row, row+1 );
        sarea[col] = dwga;
    }

    /* Make the main window die when destroy is issued. */
    g_signal_connect(window, "destroy", G_CALLBACK (gtk_main_quit), NULL);

    /* Now position the window and set its title */
    gtk_window_set_position(GTK_WINDOW(window), GTK_WIN_POS_CENTER);
    gtk_window_set_default_size(GTK_WINDOW(window), windowWidth, windowHeight);
    gtk_window_set_title(GTK_WINDOW(window), "pisensors");

    return window;
}

/*
 * ========================================================================
 * Name:   main
 *
 * Description:
 * Program startup
 * ========================================================================
 */
int
main(int argc, char *argv[])
{
    GtkWidget   *window;
    char        cwd[512];
    char        gtkrc[1024];

    struct stat stat_buf;
    char path[MAXBUF];

    // Load saved configuration and parse command line
    initConfig();
    parseArgs(argc, argv);
    validateConfig();

    // Setup logging
    piboxLoggerInit(cliOptions.logFile);
    piboxLoggerVerbosity(cliOptions.verbose);
    printf("Verbosity level: %d\n", piboxLoggerGetVerbosity());

    if ( cliOptions.logFile != NULL )
    {
        piboxLogger(LOG_INFO, "Log file: %s\n", cliOptions.logFile);
    }
    else
    {
        piboxLogger(LOG_INFO, "No log file configured.\n");
    }

    // Read environment config for keyboard behaviour
    loadKeysyms();

    /* Get display config information */
    loadDisplayConfig();
    piboxLogger(LOG_INFO, "display type: %s\n", cliOptions.display_type);
    piboxLogger(LOG_INFO, "display resolution: %s\n", cliOptions.display_resolution);

    // Set the path to the top of the image files.
    memset(path, 0, MAXBUF);
    if ( cliOptions.dbTop == NULL )
    {
        piboxLogger(LOG_INFO, "dbtop is null\n");
        if ( isCLIFlagSet( CLI_TEST) )
            sprintf(path, "./%s", DBTOP_T);
        else
            sprintf(path, "%s", DBTOP);
    }
    else
    {
        piboxLogger(LOG_INFO, "dbtop is not null\n");
        sprintf(path, "%s", cliOptions.dbTop);
    }

    // Test for db existance.
    if ( stat(path, &stat_buf) != 0 )
    {
        piboxLogger(LOG_ERROR, "Can't find database directory: %s", path);
        return(-1);
    }
    piboxLogger(LOG_INFO, "Reading database directory: %s\n", path);

    // Change to that directory
    memset(cwd, 0, 512);
    getcwd(cwd, 512);
    if ( isCLIFlagSet( CLI_TEST) )
        sprintf(gtkrc, "%s/data/gtkrc", cwd);
    else
        sprintf(gtkrc, "/etc/X11/gtkrc");

    sprintf(gtkrc, "%s/data/gtkrc", cwd);
    piboxLogger(LOG_INFO, "gtkrc: %s\n", gtkrc);
    chdir(path);

    /* Load db */
    dbLoad(".");
    maxRow = dbCount();
    chdir(cwd);

    /* 
     * If we're on a touchscreen, register the input handler.
     */
    if ( piboxGetDisplayType() == PIBOX_LCD )
    {
        piboxLogger(LOG_INFO, "Registering imageTouch.\n");
        piboxTouchRegisterCB(imageTouch, TOUCH_ABS);
        piboxTouchStartProcessor();
    }

    gtk_init(&argc, &argv);
    gtk_rc_parse(gtkrc);
    window = createWindow();
    if ( cliOptions.display_type != NULL )
        gtk_window_fullscreen (GTK_WINDOW(window));
    gtk_widget_show_all(window);

    /* Start a timer to check if we need to do UI updates. */
    lastUpdate = time(NULL);
    g_timeout_add(1000, updateUIScheduler, (gpointer) 0);

    /* Enable the watcher */
    startWatcherProcessor(path);

    piboxLogger(LOG_INFO, "Running from: %s\n", getcwd(path, MAXBUF));
    gtk_main();

    shutdownWatcherProcessor();
    if ( piboxGetDisplayType() == PIBOX_LCD )
    {
        piboxTouchShutdownProcessor();
    }
    piboxLoggerShutdown();
    return 0;
}
