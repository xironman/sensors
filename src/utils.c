/*******************************************************************************
 * pisensors
 *
 * utils.c:  Various utility methods
 *
 * License: see LICENSE file
 ******************************************************************************
 * Notes:
 * Being a C file (and not Java), please adhere to the 80 character line
 * length standard in this file.
 ******************************************************************************/
#define UTILS_C

/* To avoid deprecation warnings from GTK+ 2.x */
#pragma GCC diagnostic ignored "-Wdeprecated-declarations"

#include <stdio.h>
#include <ctype.h>
#include <string.h>
#include <sys/time.h>

#include "pisensors.h"

/*========================================================================
 * Name:   parse
 * Prototype:  int parse( char *, char **, int )
 *
 * Description:
 * Parse a character string into an array of character tokens.
 *
 * Input Arguments:
 * char *line       The string to parse
 * char **argv      The parsed tokens
 * int  max         Maximum number of arguments to parse
 *
 * Returns:
 * Number of arguments in argv.
 *
 * Note:
 * Borrowed from http://www.csl.mtu.edu/cs4411.ck/www/NOTES/process/fork/shell.c
 *========================================================================*/
int
parse(char *line, char **argv, int max)
{
    int idx = 0;

    /* if not the end of line ....... */ 
    while (*line != '\0') 
    {       
        /* replace white spaces with 0 */
        while (*line == ' ' || *line == '\t' || *line == '\n')
            *line++ = '\0';     

        /* save the argument position */
        if ( idx < max )
            *argv++ = line;

        /* skip the argument until ... */
        while (*line != '\0' && *line != ' ' && *line != '\t' && *line != '\n') 
            line++;

        if ( ++idx == max )
            break;
    }

    /* mark the end of argument list  */
    *argv = '\0';

    return idx;
}

/*========================================================================
 * Name:   getIconPath
 * Prototype:  char *getIconPath( char *name )
 *
 * Description:
 * Compute the path to the specified icon.
 *
 * Input Arguments:
 * char *name       The name of the icon to find.
 *
 * Returns:
 * Pointer to a character buffer that must be freed by the caller or NULL
 * if the name doesn't match any known icons.
 *========================================================================*/
char *
getIconPath(char *name)
{
    char    *root_path;
    char    *path = NULL;
    int     length;

    if ( isCLIFlagSet( CLI_TEST) )
        root_path = ICONS_ROOT_FD;
    else
        root_path = ICONS_ROOT_F;

    /* Error check the input */
    if ( name == NULL )
        return NULL;

    /* We don't validate the name here, just build it */
    length = strlen(root_path) + strlen(name) + 4 + 2;
    path = g_malloc0(length);
    sprintf(path, "%s/%s.png", root_path, name);

    /* Fallback to no path. */
    return path;
}
