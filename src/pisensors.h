/*******************************************************************************
 * pisensors
 *
 * pisensors.h:  program main
 *
 * License: GPL v3
 *
 ******************************************************************************
 * Notes:
 * Being a C file (and not Java), please adhere to the 80 character line
 * length standard in this file.
 ******************************************************************************/
#ifndef PISENSORS_H
#define PISENSORS_H

#include <gtk/gtk.h>

/*========================================================================
 * Globals
 *=======================================================================*/

/*========================================================================
 * Defined values
 *=======================================================================*/
#define PROG        "pisensors"

#define IMREST_PORT         8165
#define IMREST_VERSION      "1.0"

#define KEYSYMS_F           "/etc/pibox-keysyms"
#define KEYSYMS_FD          "data/pibox-keysyms"
#define ICONS_ROOT_F        "/etc/pisensors"
#define ICONS_ROOT_FD       "data/pisensors"
#define IOT_DIR_F           "/etc/ironman/iot"
#define IOT_DIR_FD          "data/iot"
#define F_CFG               "/etc/pisensors.cfg"
#define F_CFG_T             "data/pisensors.cfg"
#define F_DISPLAYCFG_T      "data/pibox-config"
#define F_DISPLAYCFG        "/etc/pibox-config"

#define MAXBUF  4096

/*========================================================================
 * Prototypes
 *=======================================================================*/
#ifndef PISENSORS_C
extern void     imageTouch( int region );
extern void     do_drawing();
extern void     updateUI();
#endif

/*========================================================================
 * Include other headers
 *=======================================================================*/
#include <pibox/log.h>
#include "db.h"
#include "cli.h"
#include "utils.h"
#include "watcher.h"

#endif /* !PISENSORS_H */

