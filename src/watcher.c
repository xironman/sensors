/*******************************************************************************
 * pisensors
 *
 * watcher.c:  Watch for changes to the IoT registration files.
 *
 * License: see LICENSE file
 ******************************************************************************
 * Notes:
 * Being a C file (and not Java), please adhere to the 80 character line
 * length standard in this file.
 ******************************************************************************/
#define WATCHER_C

/* To avoid deprecation warnings from GTK+ 2.x */
#pragma GCC diagnostic ignored "-Wdeprecated-declarations"

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <fcntl.h>
#include <pthread.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netdb.h>
#include <ifaddrs.h>
#include <string.h>
#include <errno.h>
#include <poll.h>
#include <sys/inotify.h>
#include <uuid/uuid.h>

#include <pibox/pibox.h>

#include "pisensors.h"

static char *watchDir = NULL;
static int serverIsRunning = 0;
static int serverIsEnabled = 0;
static pthread_mutex_t watcherMutex = PTHREAD_MUTEX_INITIALIZER;
static pthread_t watcherThread;

/*========================================================================
 * Name:   isWatcherProcessorEnabled
 * Prototype:  int isWatcherProcessorEnabled( void )
 *
 * Description:
 * Thread-safe read of serverIsEnabled variable.
 *========================================================================*/
static int
isWatcherProcessorEnabled( void )
{
    int status;
    pthread_mutex_lock( &watcherMutex );
    status = serverIsEnabled;
    pthread_mutex_unlock( &watcherMutex );
    return status;
}

/*========================================================================
 * Name:   setWatcherProcessorEnabled
 * Prototype:  int setWatcherProcessorEnabled( void )
 *
 * Description:
 * Thread-safe set of serverIsEnabled variable.
 *========================================================================*/
static void
setWatcherProcessorEnabled( int val )
{
    pthread_mutex_lock( &watcherMutex );
    serverIsEnabled = val;
    pthread_mutex_unlock( &watcherMutex );
}

/*========================================================================
 * Name:   isWatcherProcessorRunning
 * Prototype:  int isWatcherProcessorRunning( void )
 *
 * Description:
 * Thread-safe read of serverIsRunning variable.
 *========================================================================*/
static int
isWatcherProcessorRunning( void )
{
    int status;
    pthread_mutex_lock( &watcherMutex );
    status = serverIsRunning;
    pthread_mutex_unlock( &watcherMutex );
    return status;
}

/*========================================================================
 * Name:   setWatcherProcessorRunning
 * Prototype:  int setWatcherProcessorRunning( void )
 *
 * Description:
 * Thread-safe set of serverIsRunning variable.
 *========================================================================*/
static void
setWatcherProcessorRunning( int val )
{
    pthread_mutex_lock( &watcherMutex );
    serverIsRunning = val;
    pthread_mutex_unlock( &watcherMutex );
}

/*========================================================================
 * Name:   handle_events
 * Prototype:  int handle_events( int fd, int *wd )
 *
 * Description:
 * Handle inotify events - re: capture when an IoT registration is
 * added, removed or modified.
 *
 * Arguments:
 * int fd       File that generated the event
 * int wd       Table of directories descriptors that is being watched
 *
 *========================================================================*/
static void
handle_events(int fd, int *wd)
{
    const struct inotify_event  *event;
    ssize_t                     len;
    char                        *ptr;
    int                         doUpdates = 0;
    char                        path[MAXBUF];

    /* Size and alignment of buf[] specified by inotify(7) man page example. */
    char buf[4096] __attribute__ ((aligned(__alignof__(struct inotify_event))));

    /* Loop while events can be read from inotify file descriptor. */
    for (;;)
    {
        /* Read some events. */
        len = read(fd, buf, sizeof buf);

        /* 
         * If the nonblocking read() found no events to read, then
         * it returns -1 with errno set to EAGAIN. With errno set to
         * anything else then it's a read error. In either case
         * we exit the loop.
         */
        if (len == -1 && errno != EAGAIN)
        {
            piboxLogger(LOG_ERROR, "Error reading inotify events: %s\n", strerror(errno));
            break;
        }
        else if (len <= 0)
        {
            break;
        }

        /* Loop over all events in the buffer */
        piboxLogger(LOG_INFO, "Got an inotify events.\n");
        for (ptr = buf; ptr < buf + len; ptr += sizeof(struct inotify_event) + event->len) 
        {
            event = (const struct inotify_event *) ptr;

            /* Skip non-registration files. */
            if ( strstr(event->name,".swp") != NULL )
                continue;
            if ( strncmp(event->name, ".", 1) == 0 )
                continue;
            if ( index(event->name, '.') == NULL )
                continue;
            if ( index(event->name, '~') != NULL )
                continue;

            memset(path, 0, MAXBUF);
            snprintf(path, MAXBUF-1, "%s/%s", watchDir, event->name);

            /* If the event is on a file then handle the event. */
            if ( !(event->mask & IN_ISDIR) )
            {
                /* Print the name of the file */

                /* Print event type */
                if (event->mask & IN_CLOSE_WRITE)
                {
                    // Add the entry to the db.
                    if (event->len)
                    {
                        piboxLogger(LOG_INFO, "Close_Write event on %s\n", event->name);
                        dbReloadEntry(path, 1);
                    }
                }
                else if (event->mask & IN_DELETE)
                {
                    // Delete the entry in the db.
                    if (event->len)
                    {
                        piboxLogger(LOG_INFO, "Delete event on %s\n", event->name);
                        dbReloadEntry(path, 0);
                    }
                }

                doUpdates = 1;
            }
        }
    }

    /* Give imrest some time to make the updates before updating the UI. */
    if ( doUpdates )
    {
        sleep(3);
        updateUI();
    }
}

/*========================================================================
 * Name:   watcher
 * Prototype:  void watcher( CLI_T * )
 *
 * Description:
 * Accept and queue inbound multicast messages.
 *
 * Input Arguments:
 * void *arg    Cast to CLI_T to get run time configuration data.
 *========================================================================*/
static void *
watcher( void *arg )
{
    int             fd;
    int             *wd;
    int             poll_num;
    nfds_t          nfds;
    struct pollfd   fds[2];

    fd = inotify_init1(IN_NONBLOCK);
    if (fd == -1) 
    {
        piboxLogger(LOG_ERROR, "Failed to initialize inotify for %s: %s\n", watchDir, strerror(errno));
        pthread_exit(NULL);
    }

    /* Allocate memory for watch descriptors */
    wd = calloc(1, sizeof(int));
    if (wd == NULL) 
    {
        piboxLogger(LOG_ERROR, "Cannot allocate descriptor for %s: %s\n", watchDir, strerror(errno));
        pthread_exit(NULL);
    }

    /* 
     * Mark directories for events
     * - file was opened
     * - file was closed
     */

    wd[0] = inotify_add_watch(fd, watchDir, IN_CLOSE | IN_DELETE );
    if (wd[0] == -1) 
    {
        piboxLogger(LOG_ERROR, "Cannot watch %s: %s\n", watchDir, strerror(errno));
        pthread_exit(NULL);
    }

    piboxLogger(LOG_INFO, "Watching %s\n", watchDir);

    /* Prepare for polling */
    nfds = 1;

    /* Inotify input */
    fds[0].fd = fd;
    fds[0].events = POLLIN;

    /* Spin, waiting for connections. */
    setWatcherProcessorRunning(1);
    while ( isWatcherProcessorEnabled() )
    {
        poll_num = poll(fds, nfds, 100);
        if (poll_num == -1) 
        {
            if (errno == EINTR)
                continue;
            piboxLogger(LOG_ERROR, "Watcher poll error: %s\n", strerror(errno));
            continue;
        }

        if (poll_num > 0) 
        {
            if (fds[0].revents & POLLIN) 
            {
                /* Inotify events are available */
                handle_events(fd, wd);
            }
        }
    }

    /* Close inotify file descriptor */
    close(fd);
    free(wd);

    printf("%s: watcher thread is exiting.\n", PROG);
    setWatcherProcessorRunning(0);

    // Call return() instead of pthread_exit so valgrid doesn't report dlopen problems.
    // pthread_exit(NULL);
    return(0);
}

/*========================================================================
 * Name:   startWatcherProcessor
 * Prototype:  void startWatcherProcessor( char *path )
 *
 * Description:
 * Setup thread to handle inbound messages.
 *
 * Arguments:
 * char *path       Directory that holds IoT files to watch.
 *
 * Notes:
 * Threads run until configs->serverEnabled = 0.  See shutdownWatcherProcessor().
 *========================================================================*/
void
startWatcherProcessor(char *path)
{
    int rc;

    watchDir = g_malloc0(strlen(path)+1);
    sprintf(watchDir, "%s", path);

    /* Enable the thread loop. */
    setWatcherProcessorEnabled(1);

    /* Create a thread to handle inbound messages. */
    rc = pthread_create(&watcherThread, NULL, &watcher, (void *)NULL);
    if (rc)
    {
        piboxLogger(LOG_ERROR, "%s: Failed to create watcher thread: %s\n", PROG, strerror(rc));
        exit(-1);
    }
    piboxLogger(LOG_INFO, "%s: Started watcher thread.\n", PROG);
    return;
}

/*========================================================================
 * Name:   shutdownWatcherProcessor
 * Prototype:  void shutdownWatcherProcessor( void )
 *
 * Description:
 * Shut down message processing thread.
 *========================================================================*/
void
shutdownWatcherProcessor()
{
    int timeOut = 1;

    /* Disable the thread loop. */
    setWatcherProcessorEnabled(0);

    /* Wait for the loop to exit gracefully. */
    while ( isWatcherProcessorRunning() )
    {
        sleep(1);
        timeOut++;
        if (timeOut == 60)
        {
            DBGPrintf("%s: Timed out waiting on watcher thread to shut down.\n", PROG);
            return;
        }
    }
    pthread_detach(watcherThread);

    /* Clean up watchDir value */
    if ( watchDir != NULL )
    {
        free(watchDir);
        watchDir = NULL;
    }
    
    fprintf(stderr, "%s: watcher has shut down.\n", PROG);
}

