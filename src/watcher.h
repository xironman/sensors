/*******************************************************************************
 * pisensors
 *
 * watcher.h:  Watch for changes to the IoT registration files.
 *
 * License: see LICENSE file
 ******************************************************************************
 * Notes:
 * Being a C file (and not Java), please adhere to the 80 character line
 * length standard in this file.
 ******************************************************************************/
#ifndef WATCHER_H
#define WATCHER_H

#include <pthread.h>

/*========================================================================
 * TYPEDEFS
 *=======================================================================*/


/*========================================================================
 * Variables
 *=======================================================================*/
#ifndef WATCHER_C

extern pthread_mutex_t watcherMutex;

#endif /* !WATCHER_C */

/*========================================================================
 * Prototypes
 *=======================================================================*/
#ifndef WATCHER_C

extern void startWatcherProcessor(char *path);
extern void shutdownWatcherProcessor( void );

#endif /* !WATCHER_C */

/*========================================================================
 * Variable definitions
 *=======================================================================*/

#endif /* !WATCHER_H */
