/*******************************************************************************
 * pisensors
 *
 * db.h:  Functions for playing images in sequence
 *
 * License: GPL v3
 *
 ******************************************************************************
 * Notes:
 * Being a C file (and not Java), please adhere to the 80 character line
 * length standard in this file.
 ******************************************************************************/
#ifndef DB_H
#define DB_H

/* 
 * ========================================================================
 * Type definitions and Data structures 
 * =======================================================================
 */

/*
 * ========================================================================
 * Defined values, some of which are used in test modes only
 * =======================================================================
 */

/* Where does IoT sensor data get stored. */
#define DBTOP       "/etc/ironman/iot"

/* Test location for local media */
#define DBTOP_T     "data/iot"

/*
 * ========================================================================
 * Prototypes
 * =======================================================================
 */
#ifndef DB_C
extern void     dbLoad( char *path );
extern int      dbCount( void );
extern char     *dbGetEntry( int );
extern void     dbModifyEntry( char *entry, int delete, int state );
extern void     dbReloadEntry( const char *ipaddr, int add );
#endif

#endif /* DB_H */
